const IP_URL = "https://api.ipify.org/?format=json";
const ADDRESS_URL = "http://ip-api.com/";
const filter = "continent,country,regionName,city";

class FindByIp {
  constructor(url) {
    this.url = url;
  }

  async sendResponse(url, method = "GET", config = {}) {
    try {
      const response = await fetch(url, { method, ...config });
      if (!response.ok) throw new Error(`Fail to fetch from ${url}`);
      return response.json();
    } catch (error) {
      console.log("Error fetch data: ", error);
      throw error;
    }
  }

  async getResponse(url) {
    const dataIp = await this.sendResponse(url);
    const ip = dataIp.ip;

    const inputIP = document.querySelector("#input_ip");
    inputIP.value = ip;

    const pathWay = `${ADDRESS_URL}json/${ip}?fields=${filter}`;
    const dataIpAddress = await this.sendResponse(pathWay);
    // console.log(dataIpAddress);

    const cont = document.querySelector(".container");
    const locations = document.createElement("div");

    locations.insertAdjacentHTML(
      "beforeend",
      `
      <h3>Фактична адреса:</h3>
      <p><b>Continent: </b>${dataIpAddress.continent};</p>
      <p><b>Country: </b>${dataIpAddress.country};</p>
      <p><b>Region: </b>${dataIpAddress.regionName};</p>
      <p><b>City: </b>${dataIpAddress.city}.</p>
      `
    );

    cont.append(locations);
  }

  createHTML() {
    const container = document.createElement("div");
    container.classList = "container";

    container.insertAdjacentHTML(
      "afterbegin",
      `
        <form>
            <div class="form-group">
                <button class="btn" id="btn_get_ip" type="submit">Знайти по IP</button>
                <h3>Вихідний IP:</h3>
                <label for="input_ip">
                    <input type="text" placeholder="Отримати IP" id="input_ip" name="IP"/>
                </label>
            </div>
        </form>
        `
    );

    document.body.prepend(container);
  }

  getIP() {
    const btnGetIp = document.querySelector("#btn_get_ip");

    btnGetIp.addEventListener("click", (event) => {
      event.preventDefault();
      this.getResponse(this.url);
    });
  }

  renderDisplay() {
    this.createHTML();
    this.getIP();
  }
}

const findByIp = new FindByIp(IP_URL);
findByIp.renderDisplay();
